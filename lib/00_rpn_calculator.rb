require 'byebug'
class RPNCalculator
  # TODO: your code goes here!
  def initialize
    @operands_stack = []
  end

  def tokens(expression)
    tokens = []
    n_arr = ("0".."9").to_a
    expression.split.each do |char|
      if n_arr.include?(char)
        tokens << char.to_i
      else
        tokens << char.to_sym
      end
    end
    tokens
  end

  def evaluate(expression)
    tokens(expression).each do |token|
      if token.is_a? Integer
        push(token)
      else
        perform_operation(token)
      end
    end
    value
  end

  def push(operand)
    @operands_stack.push(operand)
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def value
    @operands_stack.last
  end

  private

  def perform_operation(op_sym)
    operands = get_operands_stack
    f_op = operands[:first_operand]
    s_op = operands[:second_operand]
    res = nil
    case op_sym
    when :+
      res = f_op + s_op
    when :-
      res = f_op - s_op
    when :/
      res = f_op.to_f / s_op
    when :*
      res = f_op * s_op
    end
    @operands_stack.push(res)
    value
  end

  def get_operands_stack
    raise "calculator is empty" if @operands_stack.length < 2
    { second_operand: @operands_stack.pop, first_operand: @operands_stack.pop }
  end
end
